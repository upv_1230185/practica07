package upv.com.phonecalls;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.telephony.SmsManager;
import android.view.View;;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {
    EditText txtCALL;
    EditText txtSMS;
    Button btnCALL;
    Button btnSMS;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        txtCALL = (EditText) findViewById(R.id.txtCALL);
        txtSMS = (EditText) findViewById(R.id.txtSMS);
        btnCALL = (Button) findViewById(R.id.btnCALL);
        btnSMS = (Button) findViewById(R.id.btnSMS);

        btnCALL.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    call(txtCALL.getText().toString());
                }
            }
        );

        btnSMS.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    sms(txtCALL.getText().toString(), txtSMS.getText().toString());
                }
            }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void call(String call) {
        startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:" + call)));
    }

    private void sms(String call, String sms) {
        SmsManager.getDefault().sendTextMessage(call, null, sms, null, null);
    }
}
